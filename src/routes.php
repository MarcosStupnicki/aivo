<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/profile/facebook/{facebook_id}[/{action}]', function (Request $request, Response $response, array $args) {
    $facebook_id = $args['facebook_id'];
    $action = $args['action'];

    /* Facebook provided this keys */
    $app_id = '{ app-id }';
    $app_secret = '{ app-secret }';
    $app_access_token = '{ app-access-token }';


    $fb = new \Facebook\Facebook([
      'app_id' => $app_id,
      'app_secret' => $app_secret,
      'default_graph_version' => 'v2.11',
      'default_access_token' => $app_access_token,
    ]);

    try {
        // Returns a `FacebookFacebookResponse` object
        $response = $fb->get('/'.$facebook_id.'?fields=id,first_name,last_name,email,picture');
    } catch(FacebookExceptionsFacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(FacebookExceptionsFacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

    $graphNode = $response->getGraphNode();

    //Check if aditional option is set
    if (isset($action) && $action != "") {
        if ($action == 'dump') {
            file_put_contents($facebook_id, $graphNode);
        }
        elseif ($action == 'log') {
            $this->logger->info("API-PROFILE-FACEBOOK /profile/facebook/ ".$facebook_id);
        }
        else{
            // Do something or nothing :P
        }
    }

    print($graphNode);
});
