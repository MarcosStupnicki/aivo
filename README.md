# Retrieve a user profile

## Objective
Build a service which retrieves the profile of one Facebook user, using the Facebook API Graph.

We expect an API endpoint which we could hit with a facebook id as parameter and the response should be all the possible information about his profile.

## Example
Request:

    GET /profile/facebook/123456

Response:

    STATUS 200
    {
        "id": 123456,
        "firstName": "Juan",
        "lastName": "Perez"
    }

## Conditions

* The project should be developed using PHP 5.4+
* If necessary, you can use any web framework of your choice, We recommend SlimPHP
* You can use the data store solution of your choice if you need one
* The full project should be correctly revisioned using GIT. That GIT repository should be accesible by us (publicly or privately) on GitHub or BitBucket.
* You don't need to serve the project to the internet but it should be testeable locally using the php built-in webserver or similar solution with the proper documentation on how to do it
* Unit Tests are a big plus!
* All added value you can give to the original idea is highly appreciated
* Have Fun!

---
## Installation
1. Clone this repo.
2. Run the following command to install project dependencies:

    ```
    aivo@aivo:~$ composer install
    ```

3. If you execute this project in an operating system based on GNU/Linux, then you will need to set the correct permissions. Execute the followings commands:

    ```
    aivo@aivo:~$ cd ..
    aivo@aivo:~$ chmod -R 777 aivo/
    ```

4. Set yours keys that Facebook was provided to you. Modify this vars: **$app_id, $app_secret, $app_access_token** in *src/routes.php*.

5. You can see it running in your webserver. Go to *path/to/project/public/*
6. Make the request to *path/to/project/public/profile/facebook/{facebook_id}*

---
## Aditionals

There are available some adtionals actions after calling API.

* You can make a request to *path/to/project/public/profile/facebook/{facebook_id}/dump* to save a current Facebook profile in public file. This will create a file with filename *facebook_id*.

    Example (request was made using this aditional action)
    ```
    aivo@aivo:~$ cat public/100000358749498
    {"id":"100000358749498","first_name":"Matias","last_name":"Diaz","picture":{"height":50,"is_silhouette":false,"url":"https:\/\/scontent.xx.fbcdn.net\/v\/t1.0-1\/p50x50\/21752318_1594956230526320_5152560090528304631_n.jpg?oh=5aef11836923e0155cd73c5577ae1630&oe=5B1EB0A6","width":50}}
    ```

* Also is avaliable *path/to/project/public/profile/facebook/{facebook_id}/log*. This request is registered in the log with a label for future searches (if necessary).

    Example (request was made using this aditional action)
    ```
    [2018-01-25 18:20:36] slim-app.INFO: API-PROFILE-FACEBOOK /profile/facebook/ 100000358749498 [] {"uid":"46610b5"}
    ```

---
## Enviroment used to develop

* PHP 7.1.13-1 (cli) (built: Jan  5 2018 12:14:37) (NTS).
* Firefox 52.5.2 (64-bits)
* Debian GNU/Linux testing (buster)
* Composer version 1.6.2
* Git version 2.15.1

---
## Contribs

Marcos Stupnicki (marcosstupnicki@gmail.com)

## Comments
* According http://phpbench.com/ i decided to use *elseif* instead *switch* when a optional parameter is called after **facebook_id**.



## Examples FB profiles id

* 100003000153025
* 100000358749498
